import Vapor

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    // Basic "It works" example
   /* router.get("/") { (req) -> Future<View> in
        return try req.view().render("list")
    }
    
    // Basic "Hello, world!" example
    router.get("api/movies") { (request) -> Future<[DemoModel]> in
        return DemoModel.query(on: request).all()
    }

    router.post(DemoModel.self, at: "api/movie") { (request, movie) -> Future<DemoModel> in
        return movie.save(on: request)
    }*/
    
    let demoController = DemoController()
    try router.register(collection: demoController)
    
    
    // Example of configuring a controller
    let todoController = TodoController()
    router.get("todos", use: todoController.index)
    router.post("todos", use: todoController.create)
    router.delete("todos", Todo.parameter, use: todoController.delete)
}
