//
//  MovieController.swift
//  App
//
//  Created by kerimcaglar on 27.12.2018.
//

import FluentSQLite
import Vapor

struct DemoController:RouteCollection{
    func boot(router: Router) throws {
        router.get("/", use: index)
        router.get("api/movies", use: getAllAPI)
        router.post(DemoModel.self, at: "api/movie", use: create)
        router.get("/add-movie", use: addMovie)
        router.post("/add-movie-db", use: addMovieDB)
    }
    
    func getAllAPI(req:Request) -> Future<[DemoModel]> {
        return DemoModel.query(on: req).all()
    }
    
    func index(req:Request) throws->Future<View>{
        return DemoModel.query(on: req).all().flatMap({ movies in
            let context = DemoContext(movies: movies)
            return try req.view().render("list", context)
        })
    }
    
    func create(req:Request, movie:DemoModel) -> Future<DemoModel> {
        return movie.save(on: req)
    }
    
    func addMovie(req:Request) throws -> Future<View> {
        return try req.view().render("create")
    }
    
    func addMovieDB(req:Request) throws -> Future<Response>{
        return try req.content.decode(TodoData.self).flatMap(to: Response.self, { data in
            print(data)
            let movie = Todo(title: data.title)
            try movie.validate()
            print(movie)
            return movie.save(on: req).map(to: Response.self, { movie in
                guard let id = movie.id else{
                    return req.redirect(to: "/")
                }
                print(id)
                return req.redirect(to: "/todos")
            })
        })
    }
}

struct DemoContext: Encodable{
    var movies:[DemoModel]?
}

struct MovieData:Content{
    let title:String
    let description:String
    let year:Int
    let duration:Int
    let rating:Double
    let imageURL:String
    let detail:Detail
}

struct TodoData:Content{
    let title:String
}

extension Todo:Validatable {
    static func validations() throws -> Validations<Todo> {
        var validations = Validations(Todo.self)
        try validations.add(\.title, .count(3...))
        return validations
    }
    
}
