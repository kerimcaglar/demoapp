//
//  DemoAppModel.swift
//  App
//
//  Created by kerimcaglar on 27.12.2018.
//

import FluentSQLite
import Vapor

final class DemoModel:Content{
    var id:Int?
    var title:String
    var description:String
    var year:Int
    var duration:Int
    var rating:Double
    var imageURL:String
    var detail:Detail
    
    init(title:String, description:String, year:Int, duration:Int, rating:Double, imageURL:String, detail:Detail) {
        self.title = title
        self.description = description
        self.year = year
        self.duration = duration
        self.rating = rating
        self.imageURL = imageURL
        self.detail = detail
    }
}

final class Detail:Content{
    var director:String
    var genres:String
    var writer:String
    
    init(director:String, genres:String, writer:String) {
        self.director = director
        self.genres = genres
        self.writer = writer
    }
}

extension DemoModel:SQLiteModel{}
extension DemoModel:Migration{}
extension DemoModel:Parameter{}

